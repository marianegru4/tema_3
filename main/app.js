const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const mysql = require('mysql2/promise')

const DB_USERNAME = 'root'
const DB_PASSWORD = 'welcome12#'

const ageStr = "age"
const nameStr = "name"
const addressStr = "address"

let conn

mysql.createConnection({
    user : DB_USERNAME,
    password : DB_PASSWORD
})
.then((connection) => {
    conn = connection
    return connection.query('CREATE DATABASE IF NOT EXISTS tw_exam')
})
.then(() => {
    return conn.end()
})
.catch((err) => {
    console.warn(err.stack)
})

const sequelize = new Sequelize('tw_exam', DB_USERNAME, DB_PASSWORD,{
    dialect : 'mysql',
    logging: false
})

let Student = sequelize.define('student', {
    name : Sequelize.STRING,
    address : Sequelize.STRING,
    age : Sequelize.INTEGER
},{
    timestamps : false
})

const app = express()
app.use(bodyParser.json())

app.get('/create', async (req, res) => {
    try{
        await sequelize.sync({force : true})
        for (let i = 0; i < 10; i++){
            let student = new Student({
                name : 'name ' + i,
                address : 'some address on ' + i + 'th street',
                age : 30 + i
            })
            await student.save()
        }
        res.status(201).json({message : 'created'})
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})
    }
})

app.get('/students', async (req, res) => {
    try{
        let students = await Student.findAll()
        res.status(200).json(students)
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})        
    }
})

function isEmpty(dict) {

    var isAgeInParams = ageStr in dict
    var isAddressInParams = addressStr in dict
    var isNameInParams = nameStr in dict
    
    if (isAgeInParams)
    {
        isAgeInParams = (dict[ageStr] !== undefined)
    }

    if (isAddressInParams)
    {
        isAddressInParams = (dict[addressStr] !== undefined)
    }

    if (isNameInParams)
    {
        isNameInParams = (dict[nameStr] !== undefined)
    }

    if (!isAddressInParams && !isAgeInParams && !isNameInParams)
    {
        return true
    }
    
    return false
}

function testMissingParams(dict)
{
    var isAgeInParams = ageStr in dict
    var isAddressInParams = addressStr in dict
    var isNameInParams = nameStr in dict

    if (!isAddressInParams || !isAgeInParams || !isNameInParams)
    {
        return true
    }
    
    return false
}

function testIncorrectAge(dict)
{
    if (dict[ageStr] < 0)
    {
        return true
    }

    return false
}

app.post('/students', async (req, res) => {
    try{

        if (isEmpty(req.body)) //se verifica body gol/undefined
        {
            res.status(400).json({message: "body is missing"})
        }
        else if (testMissingParams(req.body))//se verifica parametrii lipsa
        {
            res.status(400).json({message: "malformed request"})
        }
        else if (testIncorrectAge(req.body))//se verifica varsta negativa
        {
            res.status(400).json({message: "age should be a positive number"})
        }
        else//all gucci in body
        {
            let student = new Student(req.body)
            await sequelize.sync({force : false})
            await student.save()
            res.status(201).json({message: "created"})
        }
        
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})        
    }
})

module.exports = app